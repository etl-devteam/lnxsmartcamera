#include "appController.h"


AppController::AppController(AppView *view, AppModel *model, QString app_relative_path) :
    AbstractVisionARController(view, model, app_relative_path)
{
    static_assert(AppModel::TOF_THRESHOLD == TOFPage::TOF_THRESHOLD,
            "Please check the value of TOF_THRESHOLD in AppModel and TOFPage");
}


void AppController::connections()
{
    connect((AppModel*)model, SIGNAL(clickLED()), (AppView*)view, SLOT(clickLED()));
    connect((AppModel*)model, SIGNAL(clickTOF()), (AppView*)view, SLOT(clickTOF()));
    connect((AppModel*)model, SIGNAL(clickCAM()), (AppView*)view, SLOT(clickCAM()));

    connect((AppModel*)model, SIGNAL(LEDOffSig()),    (AppView*)view, SLOT(LEDOff()));
    connect((AppModel*)model, SIGNAL(LEDRedSig()),    (AppView*)view, SLOT(LEDRed()));
    connect((AppModel*)model, SIGNAL(LEDGreenSig()),  (AppView*)view, SLOT(LEDGreen()));
    connect((AppModel*)model, SIGNAL(LEDYellowSig()), (AppView*)view, SLOT(LEDYellow()));

    connect((AppModel*)model, SIGNAL(distanceTakenSig(int)),
            (AppView*)view,     SLOT(distanceTaken(int)));

    connect((AppModel*)model, SIGNAL(frameReady(const QImage *)),
            (AppView*)view,     SLOT(frameReady(const QImage *)));
}


void AppController::endActions()
{
}
