#include <qglobal.h>

#include "appModel.h"
#include "ledManager.h"
#include "tofManager.h"
#include "webcamManager.h"

#ifdef QT_DEBUG
#include <cstdio>
using std::printf;
#endif


AppModel::AppModel() :
    AbstractVisionARModel(),
    status       { AppStatus::WELCOME },
    choice       { OptionChoice::LED  },
    ledManager   { new LEDManager     },
    tofManager   { new TOFManager     },
    webcamManager{ new WebcamManager  }
{
}


AppModel::~AppModel()
{
    ledManager->turnOff();
    tofManager->stop();
    webcamManager->stop();
}


void AppModel::runModel()
{
    connect(ledManager.data(), &LEDManager::LEDOff, this, [this]()
    {
        emit LEDOffSig();
    });

    connect(ledManager.data(), &LEDManager::LEDGreen, this, [this]()
    {
        emit LEDGreenSig();
    });

    connect(ledManager.data(), &LEDManager::LEDYellow, this, [this]()
    {
        emit LEDYellowSig();
    });

    connect(ledManager.data(), &LEDManager::LEDRed, this, [this]()
    {
        emit LEDRedSig();
    });

    connect(webcamManager.data(), &WebcamManager::frameReady, this, [this](const QImage *img)
    {
        emit frameReady(img);
    });

    connect(tofManager.data(), &TOFManager::distanceTaken, this, &AppModel::distanceTaken);

#ifdef QT_DEBUG
    printf("\nBefore emitting first window\n");
#endif

    emit showWindow(FIRST_WINDOW);
}


void AppModel::backReleased(int /*timePressed*/)
{
    switch (status)
    {
    case AppStatus::WELCOME:
        emit newAlert(1, "QuestionCloseApp", "Return to the Launcher?");

        break;

    case AppStatus::CHOICE:
        emit showWindow(FIRST_WINDOW);
        status = AppStatus::WELCOME;

        break;

    case AppStatus::LED:
        emit showWindow(SECOND_WINDOW);
        status = AppStatus::CHOICE;

        ledManager->turnOff();

        break;

    case AppStatus::TOF:
        emit showWindow(SECOND_WINDOW);
        status = AppStatus::CHOICE;

        tofManager->stop();
        ledManager->turnOff();

        break;

    case AppStatus::WEBCAM:
        emit showWindow(SECOND_WINDOW);
        status = AppStatus::CHOICE;

#ifdef QT_DEBUG
        printf("\nback pressed in appmodel\n");
#endif

        ledManager->turnOff();
        webcamManager->stop();

#ifdef QT_DEBUG
        printf("\nafter stop in appmodel\n");
#endif

        break;

    default:
        break;
    }
}


void AppModel::okPressed()
{
    switch (status)
    {
    case AppStatus::WELCOME:
        emit showWindow(SECOND_WINDOW);
        status = AppStatus::CHOICE;

        break;

    case AppStatus::CHOICE:
        switch (choice)
        {
        case OptionChoice::LED:
            emit showWindow(THIRD_WINDOW);
            status = AppStatus::LED;

            ledManager->start();

            break;

        case OptionChoice::TOF:
            emit showWindow(FOURTH_WINDOW);
            status = AppStatus::TOF;

            ledManager->start();
            tofManager->start();

            break;

        case OptionChoice::CAM:
            emit showWindow(FIFTH_WINDOW);
            status = AppStatus::WEBCAM;

            webcamManager->start();
            ledManager->shootRed();

            break;
        }

        break;

    case AppStatus::WEBCAM:
        webcamManager->snap();

        break;

    default:
        break;
    }
}


void AppModel::forwardPressed()
{
    switch (status)
    {
    case AppStatus::CHOICE:
        switch (choice)
        {
        case OptionChoice::LED:
            emit clickTOF();
            choice = OptionChoice::TOF;

            break;

        case OptionChoice::TOF:
            emit clickCAM();
            choice = OptionChoice::CAM;

            break;

        default:
            break;
        }

        break;

    case AppStatus::LED:
        ledManager->moveRight();
        break;

    default:
        break;
    }
}


void AppModel::backwardPressed()
{
    switch (status)
    {
    case AppStatus::CHOICE:
        switch (choice)
        {
        case OptionChoice::TOF:
            emit clickLED();
            choice = OptionChoice::LED;

            break;

        case OptionChoice::CAM:
            emit clickTOF();
            choice = OptionChoice::TOF;

            break;

        default:
            break;
        }

        break;

    case AppStatus::LED:
        ledManager->moveLeft();
        break;

    default:
        break;
    }
}


void AppModel::manageAlertsEnd(QString id, bool value)
{
    if (status == AppStatus::WELCOME &&
            id == "QuestionCloseApp" &&
            value)
    {
        emit endApplication();
    }
}


void AppModel::distanceTaken(int dist)
{
    if (dist <= TOF_THRESHOLD)
    {
        ledManager->shootGreen();
    }
    else
    {
        ledManager->shootRed();
    }

    emit distanceTakenSig(dist);
}
