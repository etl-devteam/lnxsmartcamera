#include "ledManager.h"
#include "Camera/visionarcamera.h"


LEDManager::LEDManager() :
    QObject(),
    currColour{ GREEN                    },
    camera    { new VisionARCamera(0x53) }  // random number to initialize, never used
{
}


LEDManager::~LEDManager()
{
    turnOff();
}


void LEDManager::moveLeft()
{
    switch (currColour)
    {
    case RED:    currColour = OFF;    emit LEDOff();    break;
    case GREEN:  currColour = RED;    emit LEDRed();    break;
    case YELLOW: currColour = GREEN;  emit LEDGreen();  break;
    case OFF:    currColour = YELLOW; emit LEDYellow(); break;
    default:                                            break;
    }

    setLED();
}


void LEDManager::moveRight()
{
    switch (currColour)
    {
    case RED:    currColour = GREEN;  emit LEDGreen();  break;
    case GREEN:  currColour = YELLOW; emit LEDYellow(); break;
    case YELLOW: currColour = OFF;    emit LEDOff();    break;
    case OFF:    currColour = RED;    emit LEDRed();    break;
    default:                                            break;
    }

    setLED();
}


void LEDManager::setLED()
{
    camera->setLed(VisionARCamera::LedStatus(currColour));
}


void LEDManager::start()
{
    shootGreen();
}


void LEDManager::shootRed()
{
    camera->setLed(VisionARCamera::LedStatus::LED_RED);
}


void LEDManager::shootGreen()
{
    camera->setLed(VisionARCamera::LedStatus::LED_GREEN);
}


void LEDManager::turnOff()
{
    camera->setLed(VisionARCamera::LedStatus::LED_OFF);
}
