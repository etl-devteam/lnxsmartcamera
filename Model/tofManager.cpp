#include <qglobal.h>
#include "tofManager.h"
#include "Camera/visionarcamera.h"

#ifdef QT_DEBUG
    #include <cstdio>
using std::printf;
#endif


TOFManager::TOFManager() :
    QObject(),
    tof  { new VisionARCamera(VL53L1X_ADDR) },
    timer{ nullptr                          }
{
}


TOFManager::~TOFManager()
{
    if (timer != nullptr)
    {
        delete timer;
        timer = nullptr;
    }
}


void TOFManager::start()
{
    if (timer != nullptr)
    {
        delete timer;
    }

    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &TOFManager::getDistance);
    timer->start(TIMER_MS);

    VisionARCamera::DistanceThreshold th =
    {
        VisionARCamera::DISTANCE_THRESHOLD_MODE_IN, 40, 180
    };

    tof->initDevice();
    tof->setMeasurementTimingBudget(VisionARCamera::TIMING_BUDGET_100MS);
    tof->setInterMeasurementPeriod(INTER_MES_PER);
    tof->setDistanceThreshold(th);

    tof->startMeasurement();
}


void TOFManager::stop()
{
    tof->stopMeasurement();
    timer->stop();
}


void TOFManager::getDistance()
{
    const int dist = tof->getDistance();

#ifdef QT_DEBUG
    printf("\ninside tofManager.cpp, distance taken is -> %d\n", dist);
#endif

    if (dist >= 0)
    {
        emit distanceTaken(dist);
    }
}
