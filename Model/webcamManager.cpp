#include <QTimer>
#include <QDir>
#include <QImageWriter>
#include <QImage>
#include <QThread>
#include <QVariant>
#include <qglobal.h>

#include "webcamManager.h"
#include "Camera/dwebcam.h"

#ifdef QT_DEBUG
#include <cstdio>
using std::printf;
#endif


WebcamManager::WebcamManager() : QObject()
  , m_webcamThread(new QThread)
  , m_imageCaptured(nullptr)
  , webcamworkerPtr(new WebCamWorker)
{
    QDir dir(IMG_DIR);
    if (!dir.exists())
    {
        numPics = 0;
        dir.mkpath(".");
    }
    else
    {
        numPics = dir.count();
    }

    webcamworkerPtr->moveToThread(m_webcamThread);

    connect(webcamworkerPtr, &WebCamWorker::resultReady,       this,            &WebcamManager::resultReady);

    connect(m_webcamThread,  &QThread::started,                webcamworkerPtr, &WebCamWorker::threadStarted);
    connect(webcamworkerPtr, &WebCamWorker::imageCaptureEnded, m_webcamThread,  &QThread::quit);
    connect(webcamworkerPtr, &WebCamWorker::imageCaptureEnded, webcamworkerPtr, &WebCamWorker::deleteLater);
    connect(m_webcamThread,  &QThread::finished,               m_webcamThread,  &QThread::deleteLater);

#ifdef QT_DEBUG
    printf("\nWebcamManager constructed\n");
#endif
}


WebcamManager::~WebcamManager()
{
    stop();
}


void WebcamManager::start()
{
    m_webcamThread->start();
}


void WebcamManager::stop()
{
    if (m_imageCaptured != nullptr)
    {
        delete m_imageCaptured;
        m_imageCaptured = nullptr;
    }
}


void WebcamManager::resultReady(const QVariant &data)
{
    if (m_imageCaptured != nullptr) delete m_imageCaptured;

    m_imageCaptured = new QImage(data.value<QImage>());

    if (!m_imageCaptured->isNull())
    {
        emit frameReady(m_imageCaptured);
    }
}


void WebcamManager::snap()
{
#ifdef QT_DEBUG
    printf("\n Started snap \n");
#endif

    if (m_imageCaptured != nullptr && !m_imageCaptured->isNull())
    {
        QImageWriter(QString("%1/IMG_%2.jpeg").arg(IMG_DIR).arg(QString::number(++numPics)))
                .write(m_imageCaptured->mirrored(true, false));
    }

#ifdef QT_DEBUG
    printf("\n Finished snap  \n");
#endif
}
