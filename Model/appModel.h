#ifndef APPMODEL_H
#define APPMODEL_H

#include <VisionAR/Framework/abstractVisionARModel.h>

// forward declaration
class LEDManager;
class TOFManager;
class WebcamManager;


class AppModel final : public AbstractVisionARModel
{
    Q_OBJECT

public:
    AppModel();
    ~AppModel();

private:

    enum : short {
        FIRST_WINDOW = 1, SECOND_WINDOW, THIRD_WINDOW, FOURTH_WINDOW, FIFTH_WINDOW
    };

    enum class AppStatus : short {
        WELCOME, CHOICE, LED, TOF, WEBCAM
    };

    enum class OptionChoice : short {
        LED, TOF, CAM
    };

    AppStatus   status;
    OptionChoice choice;

    QScopedPointer<LEDManager>    ledManager;
    QScopedPointer<TOFManager>    tofManager;
    QScopedPointer<WebcamManager> webcamManager;

    void runModel() override;

    void backPressed() override {}
    void backReleased(int timePressed) override;

    void okPressed() override;
    void okReleased(int /*timePressed*/ = 0) override {}

    void forwardPressed() override;
    void forwardReleased(int /*timePressed*/) override {}

    void backwardPressed() override;
    void backwardReleased(int /*timePressed*/) override {}

    void singleTap() override {};
    void doubleTap() override {};

    void swipeForward() override  {};
    void swipeBackward() override {};

    void manageAlertsEnd(QString id, bool value) override;

private slots:
    void distanceTaken(int distance);

signals:
    void clickLED();
    void clickTOF();
    void clickCAM();

    void LEDOffSig();
    void LEDRedSig();
    void LEDGreenSig();
    void LEDYellowSig();

    void distanceTakenSig(int distance);

    void frameReady(const QImage *img);

public:
    static constexpr int TOF_THRESHOLD{ 100 };
};


#endif // APPMODEL_H
