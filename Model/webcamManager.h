#ifndef WEBCAMMANAGER_H
#define WEBCAMMANAGER_H

#include <QObject>

QT_FORWARD_DECLARE_CLASS(WebCamWorker)


class WebcamManager final : public QObject
{
    Q_OBJECT

public:
    WebcamManager();
    ~WebcamManager();

    void start();
    void stop();

    void snap();

private:
    static constexpr char const * const IMG_DIR{ "/home/dinex/bin/CameraPictures" };

    // members variables
    // Note: thread and worker will be deleted automatically - see the deleteLater connections in constructor
    QThread      *m_webcamThread;
    QImage       *m_imageCaptured;
    WebCamWorker *webcamworkerPtr;

    unsigned numPics;

private slots:
    void resultReady(const QVariant &data);

signals:
    void frameReady(const QImage *img);
};


#endif // WEBCAMMANAGER_H
