#ifndef __VISIONARI2C_H__
#define __VISIONARI2C_H__

#include "visionar.h"


struct VisionARI2C
{
    static int read (unsigned char saddr, unsigned char ssize, unsigned short int idx,
                     unsigned char len,   unsigned char* pval);

    static int write(unsigned char saddr, unsigned char ssize, unsigned short int idx,
                     unsigned char len,   unsigned char *pval);
};


#endif /* __VISIONARI2C_H__ */
