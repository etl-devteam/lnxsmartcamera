#ifndef DWEBCAM_H
#define DWEBCAM_H

#include <QObject>


struct buffer {
      void   *data;
      size_t  size;
};

struct RGBImage {
      unsigned char   *data; // RGB888 <=> RGB24
      size_t          width;
      size_t          height;
      size_t          size; // width * height * 3
};


class DWebCam : public QObject
{
    Q_OBJECT

public:
    explicit DWebCam(const QString& device = "/dev/video0",
                     int width = 640,
                     int height = 480,
                     QObject *parent = nullptr);

    ~DWebCam();

    /** Captures and returns a frame from the webcam.
         * Throws a runtime_error if the timeout is reached.
         */
    const RGBImage& frame(bool *bOk, int timeout = 2);

    bool start();
    void end();

//    void keyPressEvent(QKeyEvent *event);
//    void closeEvent(QCloseEvent *event);

// /////////////////////////////
private:

    void updateFrameByteSize(uint32_t width, uint32_t height, uint32_t bytesPerLine, uint32_t frameSize);
    bool initMMap();

    bool openDevice();
    bool closeDevice();

    bool initDevice();
    void uninitDevice();

    void startCapturing();
    void stopCapturing();

    bool readFrame();

    // members
    QString         m_device;
    int             m_fd;

    RGBImage        m_rgbFrame;
    buffer         *m_buffersPtr;
    unsigned int    m_nBuffers;

    size_t          m_xres, m_yres;
    size_t          m_stride;

    bool            m_forceFormat = true;
    bool            m_bStartOn = false;

    //QSerialPort *m_serial;
};



// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
// $                                       €                                            $
// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



///
/// \brief The WebCamWorker class
///

QT_FORWARD_DECLARE_CLASS(QTimer)

class WebCamWorker : public QObject
{
    Q_OBJECT

public:
    explicit WebCamWorker( QObject *parent = nullptr );
    ~WebCamWorker();

Q_SIGNALS:
    void resultReady(const QVariant &data);
    void imageCaptureEnded();

// /////////////////////////////////
public Q_SLOTS:
    void threadStarted();

private:

    void pollWorker();
    void pollWebCam();
    //void closeApplication();

    // members
    QTimer  *m_timerPtr;
    DWebCam *m_webCamPtr;
    bool    m_bThrowError;
    qint32  m_nWebCamUpdate;            // webcam detection cycles
};


#endif // DWEBCAM_H
