#include <unistd.h>

#include "alog.h"
#include "visionarcamera.h"
#include "visionari2c.h"

#define CAMERA_LED_ADDR                        0xC0
#define CAMERA_LED_IDX                         0x05

#define TIMING_BUDGET_TMOUT_MACROP_A_SM_15     0x001D
#define TIMING_BUDGET_TMOUT_MACROP_A_SM_20     0x0051
#define TIMING_BUDGET_TMOUT_MACROP_A_SM_33     0x00D6
#define TIMING_BUDGET_TMOUT_MACROP_A_SM_50     0x01AE
#define TIMING_BUDGET_TMOUT_MACROP_A_SM_100    0x02E1
#define TIMING_BUDGET_TMOUT_MACROP_A_SM_200    0x03E1
#define TIMING_BUDGET_TMOUT_MACROP_A_SM_500    0x0591

#define TIMING_BUDGET_TMOUT_MACROP_B_SM_15     0x0027
#define TIMING_BUDGET_TMOUT_MACROP_B_SM_20     0x006E
#define TIMING_BUDGET_TMOUT_MACROP_B_SM_33     0x006E
#define TIMING_BUDGET_TMOUT_MACROP_B_SM_50     0x01E8
#define TIMING_BUDGET_TMOUT_MACROP_B_SM_100    0x0388
#define TIMING_BUDGET_TMOUT_MACROP_B_SM_200    0x0496
#define TIMING_BUDGET_TMOUT_MACROP_B_SM_500    0x05C1


static VL53L1_Dev_t tof;

static const unsigned char VL53L1X_DEFAULT_CONFIG[] = {
    0x00,   // 0x2D 00: set bit 2 and 5 to 1 for fast plus mode (1MHz I2C), else don't touch
    0x00,   // 0x2E 00: bit 0 if I2C pulled up at 1.8V, else set bit 0 to 1 (pull up at AVDD)
    0x00,   // 0x2F 00: bit 0 if GPIO pulled up at 1.8V, else set bit 0 to 1 (pull up at AVDD)
    0x11,   // 0x30 11: set bit 4 to 0 for active high interrupt and 1 for active low (bits 3:0 must be 0x1)
    0x02,   // 0x31 03: bit 1 = interrupt depending on the polarity, use CheckForDataReady()
    0x00,   // 0x32 00: non modificabile
    0x02,   // 0x33 02: non modificabile
    0x08,   // 0x34 08: non modificabile
    0x00,   // 0x35 00: non modificabile
    0x08,   // 0x36 09: non modificabile
    0x10,   // 0x37 05: non modificabile
    0x01,   // 0x38 01: non modificabile
    0x01,   // 0x39 00: non modificabile
    0x00,   // 0x3A 00: non modificabile
    0x00,   // 0x3B 00: non modificabile
    0x00,   // 0x3C 00: non modificabile
    0x00,   // 0x3D 00: non modificabile
    0xFF,   // 0x3E 14: non modificabile
    0x00,   // 0x3F 8D: non modificabile
    0x0F,   // 0x40 08: non modificabile
    0x00,   // 0x41 00: non modificabile
    0x00,   // 0x42 00: non modificabile
    0x00,   // 0x43 00: non modificabile
    0x00,   // 0x44 00: non modificabile
    0x00,   // 0x45 00: non modificabile
    0x20,   // 0x46 20: interrupt configuration 0->level low detection, 1-> level high, 2-> Out of window, 3->In window, 0x20-> New sample ready , TBC
    0x0B,   // 0x47 0B: non modificabile
    0x00,   // 0x48 00: non modificabile
    0x00,   // 0x49 00: non modificabile
    0x02,   // 0x4A 02: non modificabile
    0x0A,   // 0x4B 0D: non modificabile
    0x21,   // 0x4C 21: non modificabile
    0x00,   // 0x4D 00: non modificabile
    0x00,   // 0x4E 00: non modificabile
    0x05,   // 0x4F 01: non modificabile
    0x00,   // 0x50 00: non modificabile
    0x00,   // 0x51 80: non modificabile
    0x00,   // 0x52 00: non modificabile
    0x00,   // 0x53 80: non modificabile
    0xC8,   // 0x54 8C: non modificabile
    0x00,   // 0x55 00: non modificabile
    0x00,   // 0x56 00: non modificabile
    0x38,   // 0x57 33: non modificabile
    0xFF,   // 0x58 FF: non modificabile
    0x01,   // 0x59 01: non modificabile
    0x00,   // 0x5A 00: non modificabile
    0x08,   // 0x5B 06: non modificabile
    0x00,   // 0x5C 00: non modificabile
    0x00,   // 0x5D 06: non modificabile
    0x01,   // 0x5E 02: non modificabile
    0xCC,   // 0x5F 85: non modificabile
    0x0F,   // 0x60 0B: non modificabile
    0x01,   // 0x61 01: non modificabile
    0xF1,   // 0x62 92: non modificabile
    0x0D,   // 0x63 09: non modificabile
    0x01,   // 0x64 01: Sigma threshold MSB (mm in 14.2 format for MSB+LSB), use SetSigmaThreshold(), default value 90 mm
    0x68,   // 0x65 CD: Sigma threshold LSB
    0x00,   // 0x66 00: Min count Rate MSB (MCPS in 9.7 format for MSB+LSB), use SetSignalThreshold()
    0x80,   // 0x67 20: Min count Rate LSB
    0x08,   // 0x68 08: non modificabile
    0xB8,   // 0x69 30: non modificabile
    0x00,   // 0x6A 00: non modificabile
    0x00,   // 0x6B 00: non modificabile
    0x00,   // 0x6C 00: Intermeasurement period MSB, 32 bits register, use SetIntermeasurementInMs()
    0x00,   // 0x6D 00: Intermeasurement period
    0x0F,   // 0x6E 00: Intermeasurement period
    0x89,   // 0x6F 00: Intermeasurement period LSB
    0x00,   // 0x70 00: non modificabile
    0x00,   // 0x71 00: non modificabile
    0x00,   // 0x72 00: distance threshold high MSB (in millimetri)
    0x00,   // 0x73 00: distance threshold high LSB
    0x00,   // 0x74 00: distance threshold low MSB (in millimetri)
    0x00,   // 0x75 00: distance threshold low LSB
    0x00,   // 0x76 00: non modificabile
    0x01,   // 0x77 02: non modificabile
    0x0F,   // 0x78 0B: non modificabile
    0x0D,   // 0x79 09: non modificabile
    0x0E,   // 0x7A 03: non modificabile
    0x0E,   // 0x7B 03: non modificabile
    0x00,   // 0x7C 00: non modificabile
    0x00,   // 0x7D 00: non modificabile
    0x02,   // 0x7E 00: non modificabile
    0xC7,   // 0x7F C7: ROI center, use SetROI()
    0x33,   // 0x80 FF: XY ROI (X=Width, Y=Height), use SetROI()
    0x9B,   // 0x81 FF: non modificabile
    0x00,   // 0x82 00: non modificabile
    0x00,   // 0x83 00: non modificabile
    0x00,   // 0x84 00: non modificabile
    0x01,   // 0x85 01: non modificabile
    0x00,   // 0x86 00: clear interrupt, use ClearInterrupt()
    0x00    // 0x87 00: start ranging, use StartRanging() or StopRanging(), If you want an automatic start after vl53l1x_sensor_init() call, put 0x40 in location 0x87
};

static const unsigned short VL53L1X_DEFAULT_TB[][2] = {
    { TIMING_BUDGET_TMOUT_MACROP_A_SM_15, TIMING_BUDGET_TMOUT_MACROP_B_SM_15 },
    { TIMING_BUDGET_TMOUT_MACROP_A_SM_20, TIMING_BUDGET_TMOUT_MACROP_B_SM_20 },
    { TIMING_BUDGET_TMOUT_MACROP_A_SM_33, TIMING_BUDGET_TMOUT_MACROP_B_SM_33 },
    { TIMING_BUDGET_TMOUT_MACROP_A_SM_50, TIMING_BUDGET_TMOUT_MACROP_B_SM_50 },
    { TIMING_BUDGET_TMOUT_MACROP_A_SM_100, TIMING_BUDGET_TMOUT_MACROP_B_SM_100 },
    { TIMING_BUDGET_TMOUT_MACROP_A_SM_200, TIMING_BUDGET_TMOUT_MACROP_B_SM_200 },
    { TIMING_BUDGET_TMOUT_MACROP_A_SM_500, TIMING_BUDGET_TMOUT_MACROP_B_SM_500 }
};


const char *VisionARCamera::TAG = "VAR_TOF";


VisionARCamera::VisionARCamera(unsigned char saddr) :
    dev(&tof),
    devTb(TIMING_BUDGET_15MS),
    devImp(50)
{
    ALOGV("%s: %02X", __FUNCTION__, (unsigned int)saddr);
    // device info initialization
    dev->I2cDevAddr = saddr;
}


VisionARCamera::~VisionARCamera()
{
    ALOGV("%s", __FUNCTION__);
}


int VisionARCamera::setLed(LedStatus status)
{
    unsigned char data[1];

    unsigned char result = OK_RESPONSE;

    switch (status)
    {
    case LED_OFF:    data[0] = 0xF5; break;
    case LED_RED:    data[0] = 0xF1; break;
    case LED_GREEN:  data[0] = 0xF4; break;
    case LED_YELLOW: data[0] = 0xF0; break;
    default: result = KO_RESPONSE;   break;
    }

    if (result == OK_RESPONSE)
    {
        result = VisionARI2C::write(CAMERA_LED_ADDR, 1, CAMERA_LED_IDX, sizeof(data), data);
    }

    return result;
}


VisionARCamera::LedStatus VisionARCamera::getLed(void)
{
    unsigned char data[1];

    if (VisionARI2C::read(CAMERA_LED_ADDR, 1, CAMERA_LED_IDX, sizeof(data), data) == OK_RESPONSE)
    {
        switch (data[0])
        {
        case 0xF5: return LED_OFF;
        case 0xF1: return LED_RED;
        case 0xF4: return LED_GREEN;
        case 0xF0: return LED_YELLOW;
        default:   return LED_UNK;
        }
    }

    return LED_UNK;
}


int VisionARCamera::initDevice(void)
{
    ALOGV("%s", __FUNCTION__);

    // dopo il reset, il sensore non accetta comunicazione I2C per 1.2us
    usleep(1500);

    // attende che il sensore abbia completato il boot (tempo massimo 50ms)
    if (waitDeviceBooted() == VL53L1_ERROR_NONE)
        return KO_RESPONSE;

    // invia i dati di configurazione al sensore (inizializzazione)
    for (unsigned i = 0; i < sizeof(VL53L1X_DEFAULT_CONFIG); i++)
    {
        if (VL53L1_WrByte(dev, i + 0x2D, VL53L1X_DEFAULT_CONFIG[i]) != VL53L1_ERROR_NONE)
        {
            return KO_RESPONSE;
        }
    }

    if (startMeasurement() != OK_RESPONSE)
    {
        return KO_RESPONSE;
    }

    if (waitMeasurementDataReady() != OK_RESPONSE)
    {
        return KO_RESPONSE;
    }

    if (clearInterrupt() != OK_RESPONSE)
    {
        return KO_RESPONSE;
    }

    if (stopMeasurement() != OK_RESPONSE)
    {
        return KO_RESPONSE;
    }

    if (VL53L1_WrByte(dev, VL53L1_VHV_CONFIG__TIMEOUT_MACROP_LOOP_BOUND, 0x09) != VL53L1_ERROR_NONE)
    {
        return KO_RESPONSE;
    }
    if (VL53L1_WrByte(dev, VL53L1_VHV_CONFIG__INIT, 0x00) != VL53L1_ERROR_NONE)
    {
        return KO_RESPONSE;
    }

    ALOGI(" modalita di funzionamento:  short distance mode");
    if (VL53L1_WrByte(dev, VL53L1_PHASECAL_CONFIG__TIMEOUT_MACROP, 0x14) != VL53L1_ERROR_NONE)
    {
        return KO_RESPONSE;
    }
    if (VL53L1_WrByte(dev, VL53L1_RANGE_CONFIG__VCSEL_PERIOD_A, 0x07) != VL53L1_ERROR_NONE)
    {
        return KO_RESPONSE;
    }
    if (VL53L1_WrByte(dev, VL53L1_RANGE_CONFIG__VCSEL_PERIOD_B, 0x05) != VL53L1_ERROR_NONE)
    {
        return KO_RESPONSE;
    }
    if (VL53L1_WrByte(dev, VL53L1_RANGE_CONFIG__VALID_PHASE_HIGH, 0x38) != VL53L1_ERROR_NONE)
    {
        return KO_RESPONSE;
    }
    if (VL53L1_WrWord(dev, VL53L1_SD_CONFIG__WOI_SD0, 0x0705) != VL53L1_ERROR_NONE)
    {
        return KO_RESPONSE;
    }
    if (VL53L1_WrWord(dev, VL53L1_SD_CONFIG__INITIAL_PHASE_SD0, 0x0606) != VL53L1_ERROR_NONE)
    {
        return KO_RESPONSE;
    }

#if 0
    if (setMeasurementTimingBudget(devTb) == KO_RESPONSE)
    {
        return KO_RESPONSE;
    }

    if (setInterMeasurementPeriod(devImp) == KO_RESPONSE)
    {
        return KO_RESPONSE;
    }
#endif

    return OK_RESPONSE;
}


int VisionARCamera::setMeasurementTimingBudget(VisionARCamera::TimingBudget tb)
{
    ALOGV("%s: %d", __FUNCTION__, tb);

    if (VL53L1_WrWord(dev, VL53L1_RANGE_CONFIG__TIMEOUT_MACROP_A_HI, VL53L1X_DEFAULT_TB[tb][0]) != VL53L1_ERROR_NONE)
    {
        return KO_RESPONSE;
    }
    if (VL53L1_WrWord(dev, VL53L1_RANGE_CONFIG__TIMEOUT_MACROP_B_HI, VL53L1X_DEFAULT_TB[tb][1]) != VL53L1_ERROR_NONE)
    {
        return KO_RESPONSE;
    }

    devTb = tb;
    return OK_RESPONSE;
}


int VisionARCamera::setInterMeasurementPeriod(int ms)
{
    ALOGV("%s: %d", __FUNCTION__, ms);

    int ret;
    unsigned short clock_pll;
    unsigned long l;

    ret = VL53L1_RdWord(dev, VL53L1_RESULT__OSC_CALIBRATE_VAL, &clock_pll);
    if (ret == VL53L1_ERROR_NONE)
    {
        l = ms;
        l *= clock_pll & 0x03FF;
        l *= 1064;
        l /= 1000;

        ret = VL53L1_WrDWord(dev, VL53L1_SYSTEM__INTERMEASUREMENT_PERIOD, l);
    }

    if (ret == VL53L1_ERROR_NONE)
    {
        devImp = ms;
        return OK_RESPONSE;
    }

    return KO_RESPONSE;
}


int VisionARCamera::setDistanceThreshold(VisionARCamera::DistanceThreshold th)
{
    ALOGV("%s: %d(%hu, %hu)", __FUNCTION__, th.mode, th.low, th.high);

    int ret = VL53L1_ERROR_NONE;

    if (ret == VL53L1_ERROR_NONE)
    {
        ret = VL53L1_WrByte(dev, VL53L1_SYSTEM__INTERRUPT_CONFIG_GPIO, th.mode);  // interrupt quando l'oggetto e' all'interno della finestra di misura
    }
    if (ret == VL53L1_ERROR_NONE)
    {
        ret = VL53L1_WrWord(dev, VL53L1_SYSTEM__THRESH_HIGH, th.high);     // limite lontano della finestra in millimetri   DARE LUNGHEZZA DATO
    }
    if (ret == VL53L1_ERROR_NONE)
    {
        ret = VL53L1_WrWord(dev, VL53L1_SYSTEM__THRESH_LOW, th.low);       // limite vicino della finestra in millimetri    DARE LUNGHEZZA DATO
    }

    if (ret == VL53L1_ERROR_NONE)
    {
        devTh = th;
        return OK_RESPONSE;
    }

    return KO_RESPONSE;
}


int VisionARCamera::getInterMeasurementPeriod(void) const
{
    return devImp;
}


VisionARCamera::TimingBudget VisionARCamera::getMeasurementTimingBudget(void) const
{
    return devTb;
}


VisionARCamera::DistanceThreshold VisionARCamera::getDistanceThreshold(void) const
{
    return devTh;
}


int VisionARCamera::startMeasurement(void)
{
    ALOGV("%s", __FUNCTION__);

    if (VL53L1_WrByte(dev, VL53L1_SYSTEM__MODE_START, 0x40) != VL53L1_ERROR_NONE)
    {
        return KO_RESPONSE;
    }

    if (clearInterrupt() == KO_RESPONSE)
    {
        return KO_RESPONSE;
    }

    return OK_RESPONSE;
}


int VisionARCamera::stopMeasurement(void)
{
    ALOGV("%s", __FUNCTION__);

    if (VL53L1_WrByte(dev, VL53L1_SYSTEM__MODE_START, 0x00) == VL53L1_ERROR_NONE)
    {
        return OK_RESPONSE;
    }

    return OK_RESPONSE;
}


int VisionARCamera::getDistance(void)
{
    ALOGV("%s", __FUNCTION__);

    int ret;

    unsigned char range_status;
    unsigned char status;
    unsigned short distance;

    ret = VL53L1_RdByte(dev, VL53L1_GPIO__TIO_HV_STATUS, &status);              // verifica se c'e' un interrupt
    if (ret == VL53L1_ERROR_NONE)
    {
        ret = VL53L1_RdByte(dev, VL53L1_RESULT__RANGE_STATUS, &range_status);   // verifica se la misura e' valida e...
    }

    if (ret == VL53L1_ERROR_NONE)
    {
        range_status &= 0x1F;
    }
    else
    {
        range_status = 0;
    }

    if (ret == VL53L1_ERROR_NONE)
    {
        if (range_status == 0x09)                                               // ...in tal caso...
        {
            // ...legge il valore
            ret= VL53L1_RdWord(dev, VL53L1_RESULT__FINAL_CROSSTALK_CORRECTED_RANGE_MM_SD0, &distance);
        }
        else
        {
            ret = VL53L1_ERROR_UNDEFINED;
        }
    }

    clearInterrupt();

    if (ret == VL53L1_ERROR_NONE)
    {
        return (int)distance;
    }

    return -1;
}


int VisionARCamera::waitDeviceBooted(void)
{
    ALOGV("%s", __FUNCTION__);

    if (VL53L1_WaitDeviceBooted(dev) == VL53L1_ERROR_NONE)
    {
        return OK_RESPONSE;
    }

    return KO_RESPONSE;
}


int VisionARCamera::waitMeasurementDataReady(void)
{
    ALOGV("%s", __FUNCTION__);

    if (VL53L1_WaitMeasurementDataReady(dev) == VL53L1_ERROR_NONE)
    {
        return OK_RESPONSE;
    }

    return KO_RESPONSE;
}


int VisionARCamera::clearInterrupt(void)
{
    ALOGV("%s", __FUNCTION__);

    if (VL53L1_WrByte(dev, VL53L1_SYSTEM__INTERRUPT_CLEAR, 0x01) == VL53L1_ERROR_NONE)
    {
        return OK_RESPONSE;
    }

    return OK_RESPONSE;
}
