#include "dwebcam.h"

#include <QTimer>
#include <QImage>
#include <QVariant>
#include <stdlib.h>
#include <assert.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <qglobal.h>
#include <linux/videodev2.h>

#ifdef QT_DEBUG
#include <cstdio>
using std::printf;
#endif

#define CLEAR(x) memset(&(x), 0, sizeof(x))
#define CLIP(color) (unsigned char)(((color) > 0xFF) ? 0xff : (((color) < 0) ? 0 : (color)))

#ifndef V4L2_PIX_FMT_H264
#define V4L2_PIX_FMT_H264     v4l2_fourcc('H', '2', '6', '4') /* H264 with start codes */
#endif


static int xioctl(int fh, unsigned long int request, void *arg)
{
    int r;

    do {
        r = ioctl(fh, request, arg);
    } while (-1 == r && EINTR == errno);

    return r;
}

static void v4lconvert_yuyv_to_rgb24(const unsigned char *src,
                                     unsigned char *dest,
                                     int width, int height,
                                     int stride)
{
    int j;

    while (--height >= 0) {
        for (j = 0; j + 1 < width; j += 2) {
            int u = src[1];
            int v = src[3];
            int u1 = (((u - 128) << 7) +  (u - 128)) >> 6;
            int rg = (((u - 128) << 1) +  (u - 128) +
                      ((v - 128) << 2) + ((v - 128) << 1)) >> 3;
            int v1 = (((v - 128) << 1) +  (v - 128)) >> 1;

            *dest++ = CLIP(src[0] + v1);
            *dest++ = CLIP(src[0] - rg);
            *dest++ = CLIP(src[0] + u1);

            *dest++ = CLIP(src[2] + v1);
            *dest++ = CLIP(src[2] - rg);
            *dest++ = CLIP(src[2] + u1);
            src += 4;
        }
        src += stride - (width * 2);
    }
}


DWebCam::DWebCam(const QString &device, int width, int height, QObject *parent) :
    QObject(parent),
    m_device(device),
    m_xres(width),
    m_yres(height)
{
}


DWebCam::~DWebCam()
{
    end();
}


// /////////////////////////////////////
// public
const RGBImage &DWebCam::frame(bool *bOk, int timeout)
{
    while (true)
    {
        fd_set fds;
        struct timeval tv;
        int r;

        FD_ZERO(&fds);
        FD_SET(m_fd, &fds);

        /* Timeout. */
        tv.tv_sec = timeout;
        tv.tv_usec = 0;

        r = select(m_fd + 1, &fds, nullptr, nullptr, &tv);

        if (-1 == r)
        {
            if (EINTR == errno) continue;

            *bOk = false;
            m_rgbFrame.data   = nullptr;
            m_rgbFrame.width  = 0;
            m_rgbFrame.height = 0;
            m_rgbFrame.size   = 0;

            return m_rgbFrame;
        }

        if (0 == r)
        {
#ifdef QT_DEBUG
            printf("\n%s: select timeout! \n", m_device.toStdString().c_str());
#endif
            *bOk = false;
            m_rgbFrame.data   = nullptr;
            m_rgbFrame.width  = 0;
            m_rgbFrame.height = 0;
            m_rgbFrame.size   = 0;

            return m_rgbFrame;
        }

        if (readFrame()) {
            *bOk = true;
            return m_rgbFrame;
        }

        *bOk = false;
        m_rgbFrame.data   = nullptr;
        m_rgbFrame.width  = 0;
        m_rgbFrame.height = 0;
        m_rgbFrame.size   = 0;

        return m_rgbFrame;
    }
}


bool DWebCam::start()
{
    if( m_bStartOn ) return true;

    if(!openDevice() ) {
        return false;
    }

    if( !initDevice() ) {
        end();
        return false;
    }

    // xres and yres are set to the actual resolution provided by the cam

    // frame stored as RGB888 (ie, RGB24)
    m_rgbFrame.width  = m_xres;
    m_rgbFrame.height = m_yres;
    m_rgbFrame.size   = m_xres * m_yres * 3;

    m_rgbFrame.data = new unsigned char[m_rgbFrame.size];  //(unsigned char *) malloc(m_rgbFrame.size * sizeof(char));

    startCapturing();

    m_bStartOn = true;

    return true;
}


void DWebCam::end()
{
#ifdef QT_DEBUG
printf("\nLL\n");
#endif
    m_bStartOn = false;
#ifdef QT_DEBUG
printf("\nZZ\n");
#endif
    stopCapturing();
#ifdef QT_DEBUG
printf("\nXX\n");
#endif
    uninitDevice();
#ifdef QT_DEBUG
printf("\nCC\n");
#endif
    closeDevice();
#ifdef QT_DEBUG
printf("\nVV\n");
#endif
    if (m_rgbFrame.data != nullptr)
    {
        delete[] m_rgbFrame.data;
        m_rgbFrame.data = nullptr;
    }
#ifdef QT_DEBUG
printf("\nBB\n");
#endif
}

// ////////////////////////////////////////////////
// private
bool DWebCam::initMMap()
{
    v4l2_requestbuffers req;

    CLEAR(req);

    req.count  = 4;
    req.type   = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    req.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(m_fd, VIDIOC_REQBUFS, &req))
    {
        if (EINVAL == errno)
        {
            return false;
        }
        else
        {
            return false;
        }
    }

    if (req.count < 2)
    {
        return false;
    }

    m_buffersPtr = new buffer [req.count];  //(buffer*) calloc(req.count, sizeof(buffer));

    if (m_buffersPtr == nullptr)
    {
        return false;
    }

    for (m_nBuffers = 0; m_nBuffers < req.count; ++m_nBuffers)
    {
        v4l2_buffer buf;

        CLEAR(buf);

        buf.type        = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory      = V4L2_MEMORY_MMAP;
        buf.index       = m_nBuffers;

        if (-1 == xioctl(m_fd, VIDIOC_QUERYBUF, &buf))
        {
            return false;
        }

        m_buffersPtr[m_nBuffers].size = buf.length;

        m_buffersPtr[m_nBuffers].data =
                mmap(nullptr /* start anywhere */,
                     buf.length,
                     PROT_READ | PROT_WRITE /* required */,
                     MAP_SHARED /* recommended */,
                     m_fd, buf.m.offset);

        if (MAP_FAILED == m_buffersPtr[m_nBuffers].data)
        {
            return false;
        }
    }

    return true;
}


bool DWebCam::openDevice()
{
    struct stat st;

    if (-1 == stat(m_device.toStdString().c_str(), &st)) {

        return false;

    }

    if (!S_ISCHR(st.st_mode)) {

        return false;
    }

    m_fd = open(m_device.toStdString().c_str(), O_RDWR /* required */ | O_NONBLOCK, 0);

    if (-1 == m_fd) {

        return false;
    }

    return true;
}


bool DWebCam::closeDevice()
{
    if (-1 == close(m_fd)) {

        return false;
    }

    m_fd = -1;

    return true;
}


bool DWebCam::initDevice()
{
    struct v4l2_capability cap;
    struct v4l2_cropcap cropcap;
    struct v4l2_crop crop;
    struct v4l2_format fmt;

    if (-1 == xioctl(m_fd, VIDIOC_QUERYCAP, &cap)) {
        if (EINVAL == errno) {
            return false;

        } else {

            return false;

        }
    }

    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {

        return false;

    }

    if (!(cap.capabilities & V4L2_CAP_STREAMING)) {

        return false;

    }

    /* Select video input, video standard and tune here. */


    CLEAR(cropcap);

    cropcap.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (0 == xioctl(m_fd, VIDIOC_CROPCAP, &cropcap)) {
        crop.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        crop.c = cropcap.defrect; /* reset to default */

        if (-1 == xioctl(m_fd, VIDIOC_S_CROP, &crop)) {
            switch (errno) {
            case EINVAL:
                /* Cropping not supported. */
                break;
            default:
                /* Errors ignored. */
                break;
            }
        }
    } else {
        /* Errors ignored. */
    }

    CLEAR(fmt);

    fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (m_forceFormat) {
        fmt.fmt.pix.width       = m_xres;
        fmt.fmt.pix.height      = m_yres;
        fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
        fmt.fmt.pix.field       = V4L2_FIELD_INTERLACED;

        if (-1 == xioctl(m_fd, VIDIOC_S_FMT, &fmt)) {

            return false;

        }

        if (fmt.fmt.pix.pixelformat != V4L2_PIX_FMT_YUYV ) {
            return false;

        }

        /* Note VIDIOC_S_FMT may change width and height. */
        m_xres = fmt.fmt.pix.width;
        m_yres = fmt.fmt.pix.height;

        m_stride = fmt.fmt.pix.bytesperline;


    } else {
        /* Preserve original settings as set by v4l2-ctl for example */
        if (-1 == xioctl(m_fd, VIDIOC_G_FMT, &fmt)) {

            return false;
        }
    }

    return initMMap();
}


void DWebCam::uninitDevice()
{
#ifdef QT_DEBUG
printf("\nAA\n");
#endif
    for (unsigned int i = 0; i < m_nBuffers; ++i)
    {
#ifdef QT_DEBUG
printf("\nSS%d\n", i);
#endif
        if (-1 == munmap(m_buffersPtr[i].data, m_buffersPtr[i].size))
        {
            throw std::runtime_error("munmap");
        }
    }
#ifdef QT_DEBUG
printf("\nDD\n");
#endif
    if (m_buffersPtr != nullptr)
    {
        delete [] m_buffersPtr;
        m_buffersPtr = nullptr;
    }
#ifdef QT_DEBUG
printf("\n/*-+\n");
#endif
}


void DWebCam::startCapturing()
{
    unsigned int i;
    v4l2_buf_type type;

    for (i = 0; i < m_nBuffers; ++i)
    {
        struct v4l2_buffer buf;

        CLEAR(buf);
        buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
        buf.memory = V4L2_MEMORY_MMAP;
        buf.index = i;

        if (-1 == xioctl(m_fd, VIDIOC_QBUF, &buf)) throw std::runtime_error("VIDIOC_QBUF");
    }

    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (-1 == xioctl(m_fd, VIDIOC_STREAMON, &type)) throw std::runtime_error("VIDIOC_STREAMON");
}


void DWebCam::stopCapturing()
{
    enum v4l2_buf_type type;

    type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    if (-1 == xioctl(m_fd, VIDIOC_STREAMOFF, &type)) {
        return;
    }
}


// ////////////////////////////////////////////
// private
bool DWebCam::readFrame()
{
    struct v4l2_buffer buf;

    CLEAR(buf);

    buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
    buf.memory = V4L2_MEMORY_MMAP;

    if (-1 == xioctl(m_fd, VIDIOC_DQBUF, &buf)) {
        switch (errno) {
        case EAGAIN:
            return false;

        case EIO:

        default:
            return false;

        }
    }

    assert(buf.index < m_nBuffers);

    v4lconvert_yuyv_to_rgb24((unsigned char *) m_buffersPtr[buf.index].data,
            m_rgbFrame.data,
            m_xres,
            m_yres,
            m_stride);

    if (-1 == xioctl(m_fd, VIDIOC_QBUF, &buf)) {
        return false;
    }

    return true;
}



// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
// $                                       €                                            $
// $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$



/* webcam */
#define XRES                1280
#define YRES                960
#define VIDEODEV            "/dev/video0"

/* miscellaneous */
#define	POLL_TIME            10                                      /* cycle time polling */

#define	WEBCAM_HANDLER_TIME  10                                      /* webcam management activation time */
#define WEBCAM_POLL_CYCLE    (WEBCAM_HANDLER_TIME/POLL_TIME)         /* polling cycles for webcam manager activation */

///
/// \brief WebCamWorker::WebCamWorker
/// \param parent
///
WebCamWorker::WebCamWorker(QObject *parent) : QObject( parent )
//  , m_timerPtr( new QTimer(this) )
// , m_webCamPtr( new DWebCam(VIDEODEV, XRES, YRES) )
  , m_timerPtr( nullptr )
  , m_webCamPtr( nullptr )
  , m_bThrowError( false )
  , m_nWebCamUpdate( WEBCAM_POLL_CYCLE )
{
//    m_timerPtr->setInterval( POLL_TIME );
//    QObject::connect( m_timerPtr, &QTimer::timeout, this, &WebCamWorker::pollWorker);
}


WebCamWorker::~WebCamWorker()
{
#ifdef QT_DEBUG
printf("\nnel distruttore webcamworker\n");
#endif

    if (m_timerPtr != nullptr)
    {
        m_timerPtr->stop();
        delete m_timerPtr;
    }

#ifdef QT_DEBUG
printf("\ndistrutto il timer in webcamworker\n");
#endif

    if( m_webCamPtr )
    {
        delete m_webCamPtr;
        m_webCamPtr = nullptr;
    }

#ifdef QT_DEBUG
printf("\ndistrutto tutto in webcamworker\n");
#endif
}


void WebCamWorker::threadStarted()
{
    m_webCamPtr = new DWebCam(VIDEODEV, XRES, YRES);
    m_timerPtr  = new QTimer(this);

    connect(m_timerPtr, &QTimer::timeout, this, &WebCamWorker::pollWorker);

    m_timerPtr->setInterval( POLL_TIME );
    m_timerPtr->start();
}


void WebCamWorker::pollWorker()
{
    if( --m_nWebCamUpdate == 0 )
    {
        pollWebCam();
        m_nWebCamUpdate = WEBCAM_POLL_CYCLE;
    }
}


void WebCamWorker::pollWebCam()
{
    // start/init webcam if necessary
    if( !m_webCamPtr->start() )
    {
        m_nWebCamUpdate = WEBCAM_POLL_CYCLE * 500;

        return;
    }

    bool bOk = false;
    auto frame = m_webCamPtr->frame(&bOk);
    if( bOk )
    {
        m_bThrowError = true;

        QImage aImage( frame.data,
                       frame.width,
                       frame.height,
                       QImage::Format_RGB888);

        emit resultReady( QVariant::fromValue(aImage.mirrored(true, false)) );

    }
    else
    {
        // stop capturing
        m_webCamPtr->end();
        if( m_bThrowError )
        {
            m_bThrowError = false;
            emit imageCaptureEnded();
        }
    }
}
