#ifndef __VISIONARCAMERA_H__
#define __VISIONARCAMERA_H__


#include "visionar.h"
#include "vl53l1_api.h"
#include "vl53l1_platform_user_data.h"


class VisionARCamera final
{
public:
    enum TimingBudget {
        TIMING_BUDGET_15MS,
        TIMING_BUDGET_20MS,
        TIMING_BUDGET_33MS,
        TIMING_BUDGET_50MS,
        TIMING_BUDGET_100MS,
        TIMING_BUDGET_200MS,
        TIMING_BUDGET_500MS
    };

    enum DistanceThresholdMode {
        DISTANCE_THRESHOLD_MODE_LOW,    // Trigger interrupt if value < low
        DISTANCE_THRESHOLD_MODE_HIGH,   // Trigger interrupt if value > high
        DISTANCE_THRESHOLD_MODE_OUT,    // Trigger interrupt if value < low OR value > high
        DISTANCE_THRESHOLD_MODE_IN      // Trigger interrupt if low < value < high
    };

    typedef struct PACKED _DistanceThreshold {
        DistanceThresholdMode mode;
        unsigned short int low;
        unsigned short int high;
    } DistanceThreshold;

    enum LedStatus {
        LED_OFF,
        LED_RED,
        LED_GREEN,
        LED_YELLOW,
        LED_UNK
    };

    enum class TofDistanceMode {
        TOF_SHORT = 1,
        TOF_LONG
    };

    VisionARCamera(unsigned char saddr);
    ~VisionARCamera(void);

    int initDevice(void);

    int setMeasurementTimingBudget(TimingBudget tb);
    int setInterMeasurementPeriod(int ms);
    int setDistanceThreshold(DistanceThreshold th);

    TimingBudget getMeasurementTimingBudget(void) const;
    int getInterMeasurementPeriod(void)           const;
    DistanceThreshold getDistanceThreshold(void)  const;

    int startMeasurement(void);
    int stopMeasurement(void);

    int getDistance(void);  // in millimeters

    // LED management
    int setLed(LedStatus status);
    LedStatus getLed(void);

private:
    int waitDeviceBooted(void);
    int waitMeasurementDataReady(void);
    int clearInterrupt(void);

    static const char *TAG;

    VL53L1_DEV dev;
    TimingBudget devTb;
    int devImp;
    DistanceThreshold devTh;
};


#endif /* __VISIONARCAMERA_H__ */
