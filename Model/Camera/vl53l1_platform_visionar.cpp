#include <unistd.h>
#include <time.h>
#include <arpa/inet.h>

#include "alog.h"
#include "vl53l1_platform.h"
#include "vl53l1_api.h"
#include "visionari2c.h"

#ifdef QT_DEBUG
static const char *TAG = "VAR_PLT";
#endif

template<typename T> constexpr T MIN(const T &x, const T &y) { return x < y ? x : y; }

constexpr int NUM_LOOPS_MAX { 3 };


VL53L1_Error VL53L1_RdByte(VL53L1_DEV Dev, uint16_t index, uint8_t *data) {
    VL53L1_Error retValue = VL53L1_ERROR_UNDEFINED;

    ALOGI("%s: %04hX", __FUNCTION__, index);

    for (int tries = 0; tries < NUM_LOOPS_MAX; tries++) {
        if (VisionARI2C::read(Dev->I2cDevAddr, 2, index, 1, data) == OK_RESPONSE) {
            retValue = VL53L1_ERROR_NONE;
            break;
        }
    }

    ALOGI("%s: result = %d", __FUNCTION__, retValue);

    return retValue;
}

VL53L1_Error VL53L1_WrByte(VL53L1_DEV Dev, uint16_t index, uint8_t data) {
    VL53L1_Error retValue = VL53L1_ERROR_UNDEFINED;

    ALOGI("%s: %04hX", __FUNCTION__, index);

    for (int tries = 0; tries < NUM_LOOPS_MAX; tries++) {
        if (VisionARI2C::write(Dev->I2cDevAddr, 2, index, 1, &data) == OK_RESPONSE) {
            retValue = VL53L1_ERROR_NONE;
            break;
        }
    }

    ALOGI("%s: result = %d", __FUNCTION__, retValue);

    return retValue;
}

VL53L1_Error VL53L1_RdWord(VL53L1_DEV Dev, uint16_t index, uint16_t *data) {
    VL53L1_Error retValue = VL53L1_ERROR_UNDEFINED;
    uint8_t pdata[2];

    ALOGI("%s: %04hX", __FUNCTION__, index);

    for (int tries = 0; tries < NUM_LOOPS_MAX; tries++) {
        if (VisionARI2C::read(Dev->I2cDevAddr, 2, index, 2, pdata) == OK_RESPONSE) {
            *data = pdata[0];
            *data <<= 8;
            *data |= pdata[1];
            retValue = VL53L1_ERROR_NONE;
            break;
        }
    }

    ALOGI("%s: result = %d", __FUNCTION__, retValue);

    return retValue;
}

VL53L1_Error VL53L1_RdDWord(VL53L1_DEV Dev, uint16_t index, uint32_t *data) {
    VL53L1_Error retValue = VL53L1_ERROR_UNDEFINED;
    uint8_t pdata[4];

    ALOGI("%s: %04hX", __FUNCTION__, index);

    for (int tries = 0; tries < NUM_LOOPS_MAX; tries++) {
        if (VisionARI2C::read(Dev->I2cDevAddr, 2, index, 2, pdata) == OK_RESPONSE) {
            *data = pdata[0];
            *data <<= 8;
            *data |= pdata[1];
            *data <<= 8;
            *data |= pdata[2];
            *data <<= 8;
            *data |= pdata[3];
            retValue = VL53L1_ERROR_NONE;
            break;
        }
    }

    ALOGI("%s: result = %d", __FUNCTION__, retValue);

    return retValue;
}

VL53L1_Error VL53L1_WrWord(VL53L1_DEV Dev, uint16_t index, uint16_t data) {
    VL53L1_Error retValue = VL53L1_ERROR_UNDEFINED;
    uint8_t buf[2];

    ALOGI("%s: %04hX", __FUNCTION__, index);

    buf[0] = (unsigned char)((data >> 8) & 0xFF);
    buf[1] = (unsigned char)(data & 0xFF);

    for (int tries = 0; tries < NUM_LOOPS_MAX; tries++) {
        if (VisionARI2C::write(Dev->I2cDevAddr, 2, index, 2, buf) == OK_RESPONSE) {
            retValue = VL53L1_ERROR_NONE;
            break;
        }
    }

    ALOGI("%s: result = %d", __FUNCTION__, retValue);

    return retValue;
}

VL53L1_Error VL53L1_WrDWord(VL53L1_DEV Dev, uint16_t index, uint32_t data) {
    VL53L1_Error retValue = VL53L1_ERROR_UNDEFINED;
    uint8_t buf[4];

    ALOGI("%s: %04hX", __FUNCTION__, index);

    buf[0] = (unsigned char)((data >> 24) & 0xFF);
    buf[1] = (unsigned char)((data >> 16) & 0xFF);
    buf[2] = (unsigned char)((data >> 8) & 0xFF);
    buf[3] = (unsigned char)(data & 0xFF);

    for (int tries = 0; tries < NUM_LOOPS_MAX; tries++) {
        if (VisionARI2C::write(Dev->I2cDevAddr, 2, index, 4, buf) == OK_RESPONSE) {
            retValue = VL53L1_ERROR_NONE;
            break;
        }
    }

    ALOGI("%s: result = %d", __FUNCTION__, retValue);

    return retValue;
}

VL53L1_Error VL53L1_ReadMulti(VL53L1_DEV Dev, uint16_t index, uint8_t *pdata, uint32_t count) {
    VL53L1_Error retValue = VL53L1_ERROR_UNDEFINED;

    ALOGI("%s: %04hX(%u)", __FUNCTION__, index, count);

    for (int tries = 0; tries < NUM_LOOPS_MAX; tries++) {
        if (VisionARI2C::read(Dev->I2cDevAddr, 2, index, count, pdata) == OK_RESPONSE) {
            retValue = VL53L1_ERROR_NONE;
            break;
        }
    }

    ALOGI("%s: result = %d", __FUNCTION__, retValue);

    return retValue;
}

VL53L1_Error VL53L1_WriteMulti(VL53L1_DEV Dev, uint16_t index, uint8_t *pdata, uint32_t count) {
    VL53L1_Error retValue = VL53L1_ERROR_UNDEFINED;

    ALOGI("%s: %04hX(%u)", __FUNCTION__, index, count);

    for (int tries = 0; tries < NUM_LOOPS_MAX; tries++) {
        if (VisionARI2C::write(Dev->I2cDevAddr, 2, index, count, pdata) == OK_RESPONSE) {
            retValue = VL53L1_ERROR_NONE;
            break;
        }
    }

    ALOGI("%s: result = %d", __FUNCTION__, retValue);

    return retValue;
}

VL53L1_Error VL53L1_WaitMs(VL53L1_Dev_t */*pdev*/, int32_t wait_ms){
    usleep(wait_ms*1000);

    return VL53L1_ERROR_NONE;
}

VL53L1_Error VL53L1_WaitUs(VL53L1_Dev_t */*pdev*/, int32_t wait_us){
    usleep(wait_us);

    return VL53L1_ERROR_NONE;
}

VL53L1_Error VL53L1_GetTickCount(uint32_t *ptick_count_ms)
{
    struct timespec ts;

    clock_gettime( CLOCK_REALTIME, &ts );
    *ptick_count_ms  = ts.tv_nsec / 1000000U;
    *ptick_count_ms += ts.tv_sec * 1000U;

    return VL53L1_ERROR_NONE;
}

VL53L1_Error VL53L1_WaitValueMaskEx(
    VL53L1_Dev_t *pdev,
    uint32_t      timeout_ms,
    uint16_t      index,
    uint8_t       value,
    uint8_t       mask,
    uint32_t      poll_delay_ms)
{
    uint8_t data;
    VL53L1_Error status;

    while (timeout_ms > 0)
    {
        status = VL53L1_RdByte(pdev, index, &data);
        if (status != VL53L1_ERROR_NONE) { return status; }
        if ((data & mask) == value) { return VL53L1_ERROR_NONE; }
        VL53L1_WaitMs(pdev, poll_delay_ms);
        timeout_ms -= MIN(poll_delay_ms, timeout_ms);
    }

    return VL53L1_ERROR_TIME_OUT;
}
