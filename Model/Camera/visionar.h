#ifndef _ERGLASS_H_
#define _ERGLASS_H_

#ifdef __GNUC__
#define UNUSED __attribute__ (( unused ))
#define PACKED __attribute__ (( packed ))
#endif

/* Response Type */
constexpr int OK_RESPONSE { 1 };
constexpr int KO_RESPONSE { 0 };

#endif /* _ERGLASS_H_ */
