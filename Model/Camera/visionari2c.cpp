#include <cstdio>
#include <cstring>

#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>

#include "alog.h"
#include "visionar.h"
#include "visionari2c.h"


#define INPUT_GLS                           "/dev/usb/eRGlass0"


/* Info for I2C operations */
#define I2C_LENGTH_MAX                      32
#define I2C_CONF_REG_8_BIT                  0
#define I2C_CONF_REG_16_BIT                 1

#define IOCTL_ERGLASS_CMD_I2CWRITE          0xB0
#define IOCTL_ERGLASS_CMD_I2CREAD_REQUEST   0xB2
#define IOCTL_ERGLASS_CMD_I2CREAD           0xB1


typedef struct PACKED erglass_i2cwrite {
    unsigned char       deviceaddress;
    unsigned char       deviceconfiguration;
    unsigned short int  registeraddress;
    unsigned char       numdata;
    unsigned char       data[256];
} ERGLASSI2CWRITE;

typedef struct PACKED erglass_i2cread {
    unsigned char       numdata;
    unsigned char       data[32];
} ERGLASSI2CREAD;


#ifdef QT_DEBUG
static const char *TAG = "VAR_I2C";
#endif


int VisionARI2C::write(unsigned char saddr, unsigned char ssize, unsigned short int idx, unsigned char len, unsigned char *pval)
{
    ERGLASSI2CWRITE s_i2cwrite;
    unsigned char i;
    int fd;
    int result;
    unsigned char current = 0;
    unsigned char size;

    ALOGD("%s: %02X %02X %04X %d", __FUNCTION__, saddr, ssize, idx, len);
    for (i = 0; i < len; i++) {
        ALOGD("%s: val[%04X] = %02X", __FUNCTION__, idx+i, ((unsigned int)pval[i])&0xFF);
    }

//    ALOGD("%s: opening %s...", __FUNCTION__, INPUT_GLS);
    if ((fd = open((const char *)INPUT_GLS,O_RDONLY)) < 0) return KO_RESPONSE;

    s_i2cwrite.deviceaddress = saddr;                       // indirizzo dispositivo
    if (ssize == 1) {
        s_i2cwrite.deviceconfiguration = I2C_CONF_REG_8_BIT;
    }
    else {
        s_i2cwrite.deviceconfiguration = I2C_CONF_REG_16_BIT;
    }

    while (current < len) {
        s_i2cwrite.registeraddress = idx+current;           // indirizzo registro dato dall'esterno

        if ((len - current) > I2C_LENGTH_MAX)
            size = I2C_LENGTH_MAX;
        else
            size = len - current;
        s_i2cwrite.numdata = size;

        for (i = 0; i < s_i2cwrite.numdata; i++) {
            s_i2cwrite.data[i] = pval[current+i];
        }

//        ALOGD("%s: file ioctl...", __FUNCTION__);
//        ALOGD("%s: %02X %02X %04X %d %02X",
//                __FUNCTION__,
//                ((unsigned int)s_i2cwrite.deviceaddress)&0xFF,
//                ((unsigned int)s_i2cwrite.deviceconfiguration)&0xFF,
//                ((unsigned int)s_i2cwrite.registeraddress)&0xFFFF,
//                ((unsigned int)s_i2cwrite.numdata)&0xFF,
//                ((unsigned int)s_i2cwrite.data[0])&0xFF
//                );
        if ((result = ioctl(fd, IOCTL_ERGLASS_CMD_I2CWRITE, &s_i2cwrite)) != 0) {
            ALOGE("%s: error on ioctl(IOCTL_ERGLASS_CMD_I2CWRITE): %s", __FUNCTION__, strerror(errno));
            close(fd);
            return KO_RESPONSE;
        }

        current += size;
    }

    close(fd);

    return OK_RESPONSE;
}

int VisionARI2C::read(unsigned char saddr, unsigned char ssize, unsigned short int idx, unsigned char len, unsigned char* pval)
{
    ERGLASSI2CWRITE s_i2cwrite;
    ERGLASSI2CREAD s_i2cread;
    unsigned char i;
    int fd;
    int result;
    unsigned char current = 0;
    unsigned char size;

    ALOGD("%s: %02X %02X %04X len = %d", __FUNCTION__, saddr, ssize, idx, len);

//    ALOGD("%s: opening %s...", __FUNCTION__, INPUT_GLS);
    if ((fd = open((const char *)INPUT_GLS,O_RDONLY)) < 0) return KO_RESPONSE;

    s_i2cwrite.deviceaddress = saddr | 0x01;                // indirizzo dispositivo + flag di lettura
    if (ssize == 1) {
        s_i2cwrite.deviceconfiguration = I2C_CONF_REG_8_BIT;
    }
    else {
        s_i2cwrite.deviceconfiguration = I2C_CONF_REG_16_BIT;
    }

    while (current < len) {
        s_i2cwrite.registeraddress = idx+current;           // indirizzo registro dato dall'esterno

        if ((len - current) > I2C_LENGTH_MAX)
            size = I2C_LENGTH_MAX;
        else
            size = len - current;
        s_i2cwrite.numdata = size;

        for (i = 0; i < s_i2cwrite.numdata; i++) {
            s_i2cwrite.data[i] = pval[current+i];
        }

//        ALOGD("%s: file ioctl...", __FUNCTION__);
//        ALOGD("%s: %02X %02X %04X %d",
//                __FUNCTION__,
//                ((unsigned int)s_i2cwrite.deviceaddress)&0xFF,
//                ((unsigned int)s_i2cwrite.deviceconfiguration)&0xFF,
//                ((unsigned int)s_i2cwrite.registeraddress)&0xFFFF,
//                ((unsigned int)s_i2cwrite.numdata)&0xFF
//                );
        if ((result = ioctl(fd, IOCTL_ERGLASS_CMD_I2CREAD_REQUEST, &s_i2cwrite)) != 0) {
            ALOGE("%s: error on ioctl(IOCTL_ERGLASS_CMD_I2CREAD_REQUEST): %s", __FUNCTION__, strerror(errno));
            close(fd);
            return KO_RESPONSE;
        }

        usleep(100000);

        if (ioctl(fd, IOCTL_ERGLASS_CMD_I2CREAD, &s_i2cread) < 0)
        {
            ALOGE("%s: error on ioctl(IOCTL_ERGLASS_CMD_I2CREAD): %s", __FUNCTION__, strerror(errno));
            close(fd);
            return KO_RESPONSE;
        }

        if (s_i2cread.numdata != size)
        {
            ALOGE("%s: error on I2C reading: %u bytes read", __FUNCTION__, ((unsigned int)s_i2cread.numdata & 0xFF));
            close(fd);
            return KO_RESPONSE;
        }

        for (i = 0; i < s_i2cread.numdata; i++) {
            pval[current+i] = s_i2cread.data[i];
            ALOGD("%s: val[%04X] = %02X", __FUNCTION__, (idx+current+i), ((unsigned int)pval[current+i])&0xFF);
        }

        current += size;
    }

    close(fd);

    return OK_RESPONSE;
}
