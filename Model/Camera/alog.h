#ifndef ALOG_H_
    #define ALOG_H_

    #include <qglobal.h>

    #include <libgen.h>

    #if __ANDROID__
        #include <android/log.h>

        #ifdef __cplusplus
            #include <exception>
        #endif
    #else
        #include <cstdio>
    #endif

    // Logging
    #ifdef QT_DEBUG
        #if __ANDROID__
            #define ALOGV(FMT, ...) __android_log_print(ANDROID_LOG_VERBOSE, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)
            #define ALOGD(FMT, ...) __android_log_print(ANDROID_LOG_DEBUG, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)
            #define ALOGI(FMT, ...) __android_log_print(ANDROID_LOG_INFO, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)
            #define ALOGW(FMT, ...) __android_log_print(ANDROID_LOG_WARN, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)
            #define ALOGE(FMT, ...) __android_log_print(ANDROID_LOG_ERROR, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)
            #define ALOGF(FMT, ...) __android_log_print(ANDROID_LOG_FATAL, TAG, "[%s:%d]: " FMT, basename(__FILE__), __LINE__, ## __VA_ARGS__)
        #else
            #define ALOGV(FMT, ...) printf("V %-8s: [%s:%d]: " FMT "\n", TAG, basename((char *)__FILE__), __LINE__, ## __VA_ARGS__)
            #define ALOGD(FMT, ...) printf("D %-8s: [%s:%d]: " FMT "\n", TAG, basename((char *)__FILE__), __LINE__, ## __VA_ARGS__)
            #define ALOGI(FMT, ...) printf("I %-8s: [%s:%d]: " FMT "\n", TAG, basename((char *)__FILE__), __LINE__, ## __VA_ARGS__)
            #define ALOGW(FMT, ...) printf("W %-8s: [%s:%d]: " FMT "\n", TAG, basename((char *)__FILE__), __LINE__, ## __VA_ARGS__)
            #define ALOGE(FMT, ...) printf("E %-8s: [%s:%d]: " FMT "\n", TAG, basename((char *)__FILE__), __LINE__, ## __VA_ARGS__)
            #define ALOGF(FMT, ...) printf("F %-8s: [%s:%d]: " FMT "\n", TAG, basename((char *)__FILE__), __LINE__, ## __VA_ARGS__)
        #endif
    #else
        #define ALOGV(FMT, ...)
        #define ALOGD(FMT, ...)
        #define ALOGI(FMT, ...)
        #define ALOGW(FMT, ...)
        #define ALOGE(FMT, ...)
        #define ALOGF(FMT, ...)
    #endif


    #if __ANDROID__
        //Exception
        #ifdef __cplusplus
            #define AEXCP(...) new Exception (basename(__FILE__), __LINE__, ## __VA_ARGS__)
        #endif
    #endif

#endif /* ALOG_H_ */
