#ifndef TOFMANAGER_H
#define TOFMANAGER_H

#include <QObject>
#include <QTimer>


// forward declaration
class VisionARCamera;


class TOFManager final : public QObject
{
    Q_OBJECT

public:
    TOFManager();
    ~TOFManager();

    void start();
    void stop();

private:
    static constexpr uint8_t VL53L1X_ADDR { 0x53 };
    static constexpr int     TIMER_MS     { 333  };
    static constexpr int     INTER_MES_PER{ 500  };

    QScopedPointer<VisionARCamera> tof;

    QTimer *timer;

private slots:
    void getDistance();

signals:
    void distanceTaken(int dist);
};


#endif // TOFMANAGER_H
