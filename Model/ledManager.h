#ifndef LEDMANAGER_H
#define LEDMANAGER_H

#include <QObject>


// forward declaration
class VisionARCamera;


class LEDManager final : public QObject
{
    Q_OBJECT

public:
    LEDManager();
    ~LEDManager();

    void moveRight();
    void moveLeft();

    void start();
    void turnOff();

    void shootGreen();
    void shootRed();

private:
    enum Colour {
        OFF, RED, GREEN, YELLOW
    };

    Colour currColour;

    QScopedPointer<VisionARCamera> camera;

    void setLED();

signals:
    void LEDOff();
    void LEDRed();
    void LEDGreen();
    void LEDYellow();
};


#endif  // LEDMANAGER_H
