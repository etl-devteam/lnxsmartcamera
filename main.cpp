#include <QApplication>
#include <QScopedPointer>
#include <qglobal.h>

#include "Controller/appController.h"



#ifdef QT_DEBUG

/// code taken from
/// https://stackoverflow.com/questions/77005/how-to-automatically-generate-a-stacktrace-when-my-program-crashes

#include <stdio.h>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>


void handler(int sig) {
  void *array[10];
  size_t size;

  // get void*'s for all entries on the stack
  size = backtrace(array, 10);

  // print out all the frames to stderr
  fprintf(stderr, "Error: signal %d:\n", sig);
  backtrace_symbols_fd(array, size, STDERR_FILENO);
  exit(1);
}

#endif


int main(int argc, char *argv[])
{
#ifdef QT_DEBUG
    signal(SIGSEGV, handler);   // install our handler
#endif

    QScopedPointer<QApplication> a(new QApplication(argc, argv));

    AppController::startCheckingGlasses();

    QScopedPointer<AppModel> model(new AppModel);
    QScopedPointer<AppView>  view (new AppView);

    AppController controller(view.data(), model.data(), "SmartCamera/SmartCamera");

    return a->exec();
}
