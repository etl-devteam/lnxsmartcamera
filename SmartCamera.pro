QT       += core gui xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

TARGET = SmartCamera
TEMPLATE = app


# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

QMAKE_CXXFLAGS_DEBUG += \
                -rdynamic \
                -funwind-tables

INCLUDEPATH += \
    Model/Camera/api/platform \
    Model/Camera/api/core

SOURCES += \
    Controller/appController.cpp \
    Model/Camera/api/core/vl53l1_api.c \
    Model/Camera/api/core/vl53l1_api_calibration.c \
    Model/Camera/api/core/vl53l1_api_core.c \
    Model/Camera/api/core/vl53l1_api_debug.c \
    Model/Camera/api/core/vl53l1_api_preset_modes.c \
    Model/Camera/api/core/vl53l1_api_strings.c \
    Model/Camera/api/core/vl53l1_core.c \
    Model/Camera/api/core/vl53l1_core_support.c \
    Model/Camera/api/core/vl53l1_error_strings.c \
    Model/Camera/api/core/vl53l1_register_funcs.c \
    Model/Camera/api/core/vl53l1_silicon_core.c \
    Model/Camera/api/core/vl53l1_wait.c \
    Model/Camera/dwebcam.cpp \
    Model/Camera/visionarcamera.cpp \
    Model/Camera/visionari2c.cpp \
    Model/Camera/vl53l1_platform_visionar.cpp \
    Model/appModel.cpp \
    Model/ledManager.cpp \
    Model/tofManager.cpp \
    Model/webcamManager.cpp \
    View/appView.cpp \
    View/choicePage.cpp \
    View/ledPage.cpp \
    View/tofPage.cpp \
    View/webcamPage.cpp \
    View/welcomePage.cpp \
    main.cpp

HEADERS += \
    Controller/appController.h \
    Model/Camera/alog.h \
    Model/Camera/api/core/vl53l1_api.h \
    Model/Camera/api/core/vl53l1_api_calibration.h \
    Model/Camera/api/core/vl53l1_api_core.h \
    Model/Camera/api/core/vl53l1_api_debug.h \
    Model/Camera/api/core/vl53l1_api_preset_modes.h \
    Model/Camera/api/core/vl53l1_api_strings.h \
    Model/Camera/api/core/vl53l1_core.h \
    Model/Camera/api/core/vl53l1_core_support.h \
    Model/Camera/api/core/vl53l1_def.h \
    Model/Camera/api/core/vl53l1_error_codes.h \
    Model/Camera/api/core/vl53l1_error_exceptions.h \
    Model/Camera/api/core/vl53l1_error_strings.h \
    Model/Camera/api/core/vl53l1_ll_def.h \
    Model/Camera/api/core/vl53l1_ll_device.h \
    Model/Camera/api/core/vl53l1_nvm_map.h \
    Model/Camera/api/core/vl53l1_preset_setup.h \
    Model/Camera/api/core/vl53l1_register_funcs.h \
    Model/Camera/api/core/vl53l1_register_map.h \
    Model/Camera/api/core/vl53l1_register_settings.h \
    Model/Camera/api/core/vl53l1_register_structs.h \
    Model/Camera/api/core/vl53l1_silicon_core.h \
    Model/Camera/api/core/vl53l1_tuning_parm_defaults.h \
    Model/Camera/api/core/vl53l1_wait.h \
    Model/Camera/api/platform/vl53l1_platform.h \
    Model/Camera/api/platform/vl53l1_platform_log.h \
    Model/Camera/api/platform/vl53l1_platform_user_config.h \
    Model/Camera/api/platform/vl53l1_platform_user_data.h \
    Model/Camera/api/platform/vl53l1_platform_user_defines.h \
    Model/Camera/api/platform/vl53l1_types.h \
    Model/Camera/dwebcam.h \
    Model/Camera/visionar.h \
    Model/Camera/visionarcamera.h \
    Model/Camera/visionari2c.h \
    Model/appModel.h \
    Model/ledManager.h \
    Model/tofManager.h \
    Model/webcamManager.h \
    View/appView.h \
    View/choicePage.h \
    View/ledPage.h \
    View/tofPage.h \
    View/webcamPage.h \
    View/welcomePage.h

FORMS += \
    View/webcam.ui \
    View/welcome.ui \
    View/choice.ui \
    View/led.ui \
    View/tof.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

LIBS += -lBtnsEventGen2 -lGlassesCheck -lTouchEventsGen2 -lVisionARAlertFactory \
        -lAbstractVisionARController -lAbstractVisionARModel -lAbstractVisionARView \
        -lThreadObjInterface -lBtnsInterface -lBatteryManager -lAlertsManager -lLauncherAdapterInterface -lTouchInterface

RESOURCES += \
    resource.qrc
