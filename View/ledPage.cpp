#include "ledPage.h"


LEDPage::LEDPage() :
    ledPage{ new Ui_LED_page }
{
}


LEDPage::~LEDPage()
{
    delete ledPage;
    ledPage = nullptr;
}


void LEDPage::setupUi(QWidget *widget)
{
    ledPage->setupUi(widget);
}


void LEDPage::LEDOff()
{
    ledPage->labelR->setFrameShape(QFrame::NoFrame);
    ledPage->labelY->setFrameShape(QFrame::NoFrame);
}


void LEDPage::LEDRed()
{
    ledPage->labelR->setFrameShape(QFrame::Box);
    ledPage->labelG->setFrameShape(QFrame::NoFrame);
}


void LEDPage::LEDGreen()
{
    ledPage->labelR->setFrameShape(QFrame::NoFrame);
    ledPage->labelY->setFrameShape(QFrame::NoFrame);
    ledPage->labelG->setFrameShape(QFrame::Box);
}


void LEDPage::LEDYellow()
{
    ledPage->labelY->setFrameShape(QFrame::Box);
    ledPage->labelG->setFrameShape(QFrame::NoFrame);
}
