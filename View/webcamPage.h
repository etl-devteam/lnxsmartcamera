#ifndef WEBCAMPAGE_H
#define WEBCAMPAGE_H

#include "ui_webcam.h"


class WebcamPage final
{
public:
    WebcamPage();
    ~WebcamPage();

    void setupUi(QWidget *widget);

    void frameReady(const QImage *img);

private:
    Ui_Webcam_page *webcamPage;
};


#endif // WEBCAMPAGE_H
