#ifndef LEDPAGE_H
#define LEDPAGE_H

#include "ui_led.h"


class LEDPage final
{
public:
    LEDPage();
    ~LEDPage();

    void setupUi(QWidget *widget);

private:
    Ui_LED_page *ledPage;

public:
    void LEDOff();
    void LEDRed();
    void LEDGreen();
    void LEDYellow();
};


#endif  // LEDPAGE_H
