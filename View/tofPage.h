#ifndef TOFPAGE_H
#define TOFPAGE_H

#include "ui_tof.h"


class TOFPage final
{
public:
    TOFPage();
    ~TOFPage();

    void setupUi(QWidget *widget);

    void setDistance(int dist);

private:

    Ui_TOF_page *tofPage;

    QPixmap picInside;
    QPixmap picOutside;

public:
    static constexpr int TOF_THRESHOLD{ 100 };
};


#endif  // TOFPAGE_H
