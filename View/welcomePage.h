#ifndef WELCOMEPAGE_H
#define WELCOMEPAGE_H

#include "ui_welcome.h"


class WelcomePage final
{
public:
    WelcomePage();
    ~WelcomePage();

    void setupUi(QWidget *widget);

private:
    Ui_Welcome_page *welcomePage;
};


#endif  // WELCOMEPAGE_H
