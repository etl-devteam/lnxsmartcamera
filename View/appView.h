#ifndef APPVIEW_H
#define APPVIEW_H

#include <VisionAR/Framework/abstractVisionARView.h>

#include "welcomePage.h"
#include "choicePage.h"
#include "ledPage.h"
#include "tofPage.h"
#include "webcamPage.h"


class AppView final : public AbstractVisionARView
{
    Q_OBJECT

public:
    AppView();

private:
    QScopedPointer<WelcomePage> welcomePage;
    QScopedPointer<ChoicePage>  choicePage;
    QScopedPointer<LEDPage>     ledPage;
    QScopedPointer<TOFPage>     tofPage;
    QScopedPointer<WebcamPage>  webcamPage;

public slots:
    void clickLED();
    void clickTOF();
    void clickCAM();

    void LEDOff();
    void LEDRed();
    void LEDGreen();
    void LEDYellow();

    void distanceTaken(int dist);

    void frameReady(const QImage *img);
signals:
};


#endif  // APPVIEW_H
