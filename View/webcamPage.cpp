#include "webcamPage.h"


WebcamPage::WebcamPage() :
    webcamPage{ new Ui_Webcam_page }
{
}


WebcamPage::~WebcamPage()
{
    delete webcamPage;
    webcamPage = nullptr;
}


void WebcamPage::setupUi(QWidget *widget)
{
    webcamPage->setupUi(widget);
}


void WebcamPage::frameReady(const QImage *img)
{
    if (img != nullptr)
    {
        webcamPage->labelImg->setPixmap(QPixmap::fromImage(img->mirrored(true, false))
                                        .scaled(235, 133, Qt::KeepAspectRatio));
    }
}
