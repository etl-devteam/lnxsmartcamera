#ifndef CHOICEPAGE_H
#define CHOICEPAGE_H

#include "ui_choice.h"


class ChoicePage final
{
public:
    ChoicePage();
    ~ChoicePage();

    void setupUi(QWidget *widget);

    void checkLED();
    void checkTOF();
    void checkCAM();

private:
    Ui_Choice_page *choicePage;
};


#endif  // CHOICEPAGE_H
