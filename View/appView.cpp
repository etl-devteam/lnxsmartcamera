#include "appView.h"


AppView::AppView() :
    AbstractVisionARView(),
    welcomePage{ new WelcomePage },
    choicePage { new ChoicePage  },
    ledPage    { new LEDPage     },
    tofPage    { new TOFPage     },
    webcamPage { new WebcamPage  }
{
    // the order in which you "register window"s here
    // matches the number you pass in "showWindow(int)"

    registerWindow(welcomePage.data());
    registerWindow(choicePage.data());
    registerWindow(ledPage.data());
    registerWindow(tofPage.data());
    registerWindow(webcamPage.data());
}


void AppView::LEDOff()
{
    ledPage->LEDOff();
}


void AppView::LEDRed()
{
    ledPage->LEDRed();
}


void AppView::LEDGreen()
{
    ledPage->LEDGreen();
}


void AppView::LEDYellow()
{
    ledPage->LEDYellow();
}


void AppView::distanceTaken(int dist)
{
    tofPage->setDistance(dist);
}


void AppView::clickLED()
{
    choicePage->checkLED();
}


void AppView::clickTOF()
{
    choicePage->checkTOF();
}


void AppView::clickCAM()
{
    choicePage->checkCAM();
}


void AppView::frameReady(const QImage *img)
{
    webcamPage->frameReady(img);
}
