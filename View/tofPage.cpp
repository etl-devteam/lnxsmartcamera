#include <qglobal.h>
#include "tofPage.h"

#ifdef QT_DEBUG
    #include <cstdio>
using std::printf;
#endif


TOFPage::TOFPage() :
    tofPage{ new Ui_TOF_page }
{
}


TOFPage::~TOFPage()
{
    delete tofPage;
    tofPage = nullptr;
}


void TOFPage::setupUi(QWidget *widget)
{
    tofPage->setupUi(widget);

    picInside  = QPixmap(":/Images/inside.png").scaled(tofPage->scaleImg->size(),
                                                       Qt::KeepAspectRatio, Qt::SmoothTransformation);
    picOutside = QPixmap(":/Images/outside.png").scaled(tofPage->scaleImg->size(),
                                                        Qt::KeepAspectRatio, Qt::SmoothTransformation);
}


void TOFPage::setDistance(int dist)
{

#ifdef QT_DEBUG
printf("\ninside setDistance in tofPage.cpp, dist is -> %d\n", dist);
#endif

    tofPage->distanceNumber->setText(QString::number(dist));

    if (dist <= TOF_THRESHOLD)
    {
        tofPage->scaleImg->setPixmap(picInside);
    }
    else
    {
        tofPage->scaleImg->setPixmap(picOutside);
    }
}
