#include "choicePage.h"


ChoicePage::ChoicePage() :
    choicePage{ new Ui_Choice_page }
{
}


ChoicePage::~ChoicePage()
{
    delete choicePage;
    choicePage = nullptr;
}


void ChoicePage::setupUi(QWidget *widget)
{
    choicePage->setupUi(widget);
}


void ChoicePage::checkLED()
{
    choicePage->checkBoxLED->setChecked(true);
    choicePage->checkBoxTOF->setChecked(false);
}


void ChoicePage::checkTOF()
{
    choicePage->checkBoxLED->setChecked(false);
    choicePage->checkBoxTOF->setChecked(true);
    choicePage->checkBoxCAM->setChecked(false);
}


void ChoicePage::checkCAM()
{
    choicePage->checkBoxCAM->setChecked(true);
    choicePage->checkBoxTOF->setChecked(false);
}
